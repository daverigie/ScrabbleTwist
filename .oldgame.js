
// EXTEND THE CREATEJS CONTAINER CLASS TO CREATE A TILE OBJECT

(function() { 
function Tile(letter, x0, y0){

	this.Container_constructor();
	this.x = x0;
	this.y = y0;
	this.letter = letter;
	this.score = TILE_VALUES[letter];
	this.setup();
}

var p = createjs.extend(Tile, createjs.Container);

p.setup = function(){

	// Add letter to tile square
	var tileface = new createjs.Shape();
	tileface.graphics.beginFill(TILE_FILL_COLOR).drawRect(0, 
														  0, 
														  TILE_WIDTH, 
														  TILE_WIDTH)
	tileface.graphics.beginStroke(TILE_BORDER_COLOR).drawRect(0,
															  0,
															  TILE_WIDTH,
															  TILE_WIDTH);

	this.addChild(tileface);

	var letterfont = TILE_LETTER_WIDTH.toString() + "px " + TILE_LETTER_FONT;
	var letterface = new createjs.Text(this.letter,
									   letterfont,
									   TILE_LETTER_COLOR);

	// position relative to tile
	letterface.x = TILE_WIDTH/2.0;
	letterface.y = TILE_WIDTH/2.0;
	letterface.textAlign = "center";
	letterface.textBaseline = "middle";

    this.addChild(letterface); 

    // add score to bottom right corner
    var scorefont = (TILE_LETTER_WIDTH/4.0).toString() + "px " + TILE_LETTER_FONT;
    var scoreface = new createjs.Text(this.score.toString(), 
    								  scorefont,
    								  TILE_LETTER_COLOR);

    scoreface.x = TILE_WIDTH*0.75;
    scoreface.y = scoreface.x;
    this.addChild(scoreface);


}

p.move = function(x,y){

	var z = this.parent.children.length -1;
	this.parent.setChildIndex(this, z);
	//this.x = x;
	//this.y = y;
	//stage.update();

	createjs.Tween.get(this)
	  .to({ x: x, y: y }, 200, createjs.Ease.getPowInOut(2));

}


window.Tile = createjs.promote(Tile, "Container");

}());

// EXTEND THE CREATEJS CONTAINER CLASS TO MAKE A TILEBOARD OBJECT (USED FOR RACK AND BOARD)

(function() {
function TileBoard(word, x0, y0){

	this.Container_constructor();
	this.x = x0;
	this.y = y0;
	this.word = word;
	this.tiles = [];
	this.boardspaces = [];
	this.played_tiles = [];
	this.unplayed_tiles = [];
	this.setup_spaces();
	this.setup_rack();
	
}

var p = createjs.extend(TileBoard, createjs.Container);

p.setup_rack = function(){

	word = this.word.shuffle();
	for (var i=0; i<word.length; i++){
		var x = i*(TILE_WIDTH + TILE_SPACING);
		var y = BOARDSPACE_SEP;
		var tile = new Tile(word[i], x, y);
		this.tiles.push(tile)
		this.unplayed_tiles.push(tile);
		this.addChild(tile);
	}

}

p.setup_spaces = function(){

	N = this.word.length;

	for (var i=0; i<N; i++){

		var x = i*(TILE_WIDTH+TILE_BOX_BORDER_WIDTH);
		var y = 0;

		var boardspace = new createjs.Shape();
		boardspace.graphics.beginFill(TILE_BOX_FILL_COLOR).drawRect(0, 
															        0, 
															        TILE_WIDTH, 
															        TILE_WIDTH);

		boardspace.graphics.setStrokeStyle(TILE_BOX_BORDER_WIDTH);
		var W = TILE_WIDTH + TILE_BOX_BORDER_WIDTH;
		boardspace.graphics.beginStroke(TILE_BOX_BORDER_COLOR).drawRect(0, 
															   0, 
															   W, 
										                       W);
		
		boardspace.x = x;
		boardspace.y = y;
		this.boardspaces.push(boardspace);
		this.addChild(boardspace);
	}
}

p.select_tile = function(letter){
	createjs.Sound.play(soundID_select_tile);
	tile = this.getTile(this.unplayed_tiles, letter);
	if (tile){
		ind = this.unplayed_tiles.indexOf(tile);
		this.unplayed_tiles.splice(ind, 1);
		this.played_tiles.push(tile);

		coords = this.getSpaceCoords(this.played_tiles.length-1);
		x = coords[0]; y = coords[1];
		tile.move(x,y);
		return 1;
	}	
	return 0;
}

p.spacepop = function(playsound){
	// Pops the last placed tile back to where it came from

	if (playsound === undefined){
		playsound = true;
	}

	if (playsound){
		createjs.Sound.play(soundID_select_tile);
	}
	L = this.played_tiles.length;
	if (L > 0){
		tile = this.played_tiles.pop();
		this.unplayed_tiles.push(tile);
		ind = this.tiles.indexOf(tile);
	    coords = this.getRackCoords(ind);
	    x = coords[0]; y = coords[1];
	    tile.move(x,y);
	}
}

p.shuffle = function(){
	createjs.Sound.play(soundID_shuffle_tiles);
	var j = 0;
	this.unplayed_tiles.shuffle();
	var shuffled_tiles = [];
	for (var i=0; i<this.tiles.length; i++){
		if (this.unplayed_tiles.indexOf(this.tiles[i]) > -1){
			shuffled_tiles.push(this.unplayed_tiles[j]);
			j += 1;

		}
		else{
			shuffled_tiles.push(this.tiles[i]);
		}
		
	}

	this.tiles = shuffled_tiles

	for (var i=0; i<this.unplayed_tiles.length; i++){
		var ind = this.tiles.indexOf(this.unplayed_tiles[i]);
		coords = this.getRackCoords(ind);
		x = coords[0]; y = coords[1];
		this.unplayed_tiles[i].move(x,y);
	}
}

p.play = function(){
	createjs.Sound.play(soundID_submit_word);
	while (this.played_tiles.length>0){
		this.spacepop(false);
	}
}

p.getSpaceCoords = function(i){

	x = this.boardspaces[i].x;
	y = this.boardspaces[i].y;

	x += TILE_BOX_BORDER_WIDTH/2.0;
	y += TILE_BOX_BORDER_WIDTH/2.0;

	return [x, y];
}

p.getRackCoords = function(i){

	var y = BOARDSPACE_SEP;
	var x = i*(TILE_WIDTH + TILE_SPACING);
	return [x,y];
}

p.getTile = function(tile_arr, letter){

	for (var i=0; i<tile_arr.length; i++){
		if (tile_arr[i].letter == letter){
			return tile_arr[i];
		}
	}

	return null;

}

p.getString = function(tile_arr){
	s = ""
	for (var i=0; i<tile_arr.length; i++){
		s = s + tile_arr[i].letter;
	}
	return s
}

p.getWidth = function(){
	return (this.word.length)*TILE_WIDTH + (this.word.length-1)*TILE_SPACING;
}

p.getHeight = function(){
	return TILE_WIDTH;
}

window.TileBoard = createjs.promote(TileBoard, "Container");

}());

function Game(){
	
	this.score = 0.0;
	this.round = 1;
	this.words7 = getWordsOfLength(WORDLIST, 7);
	this.setup();

}
Game.prototype.setup = function(){

	    // load sounds
	    this.loadSounds();		

	    window.addEventListener( "keydown", this.doKeyDown, false );

		// Get random 7 letter word from dictionary
		this.currentWord = this.randomWord();

		rack = new TileBoard(this.currentWord, 0, 0);
		rack.x = (c.width - rack.getWidth())/2;
		rack.y = (c.height - rack.getHeight())/2;
		stage.addChild(rack);


	}


Game.prototype.randomWord = function(){
		return this.words7.draw();
	}

Game.prototype.loadSounds = function() {
	  createjs.Sound.registerSound("Sounds/select_tile.wav", soundID_select_tile);
	  createjs.Sound.registerSound("Sounds/shuffle_tiles.wav", soundID_shuffle_tiles);
	  createjs.Sound.registerSound("Sounds/submit_word.wav", soundID_submit_word);
	}

Game.prototype.doKeyDown = function(e) {
	
		console.log(e.keyCode);

		if (e.keyCode === 8){ //BACKSPACE
			rack.spacepop();
			e.preventDefault();
			
		}

		else if (e.keyCode == 13){ //ENTER
			rack.play();
		}
		else if (e.keyCode == 32){
			rack.shuffle();
		} 
		else{
			// SPACE

			key_pressed = e.keyCode;
			key_pressed = String.fromCharCode(key_pressed);
			key_pressed = key_pressed.toUpperCase();
			rack.select_tile(key_pressed);
			console.log(key_pressed);
	}

}



var soundID_select_tile = "sound_select_tile";
var soundID_shuffle_tiles = "sound_shuffle_tiles";
var soundID_submit_word = "sound_submit_word";

var c = document.getElementById("myCanvas");
c.style.background = "#CF0000";
var stage = new createjs.Stage(c);

game = new Game();


createjs.Ticker.setFPS(60);
createjs.Ticker.addEventListener("tick", stage);

//stage.update();



