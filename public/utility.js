Array.prototype.shuffle = function() {
  var i = this.length, j, temp;
  if ( i == 0 ) return this;
  while ( --i ) {
     j = Math.floor( Math.random() * ( i + 1 ) );
     temp = this[i];
     this[i] = this[j];
     this[j] = temp;
  }
  return this;
}

Array.prototype.draw = function(){
	var ind = Math.floor(Math.random()*this.length);
	return this[ind];
}

String.prototype.shuffle = function () {
    var a = this.split(""),
        n = a.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}

String.prototype.repeat = function(times) {
   return (new Array(times + 1)).join(this);
};

function isPrime(x){

	if (x==1){
		return false;
	}
	else if (x==2){

		return true;
	}
	else{

		var y = 2;
		while (y <= Math.sqrt(x)){
			if (x%y == 0){
				return false;
			}
			y++;
		}
	}

	return true;

}

function getPrimes(N){

	var counter = 0;
	var i = 2;
	var primes = new Array(N);

	while (counter < N){
		if (isPrime(i)){
			primes[counter] = i;
			counter++;
		}
		i ++		
	}

	return primes
}

function compareWords(word1, word2){

	if (word1.length == word2.length){
		return (word1>word2);
	}

	return word1.length - word2.length;

	

}

function getPrimeCode(word){

	var primeCode = 1;
	for (var i=0; i<word.length; i++){
		primeCode *= PRIME_CODES[word[i]];
	}	

	return primeCode;
}

function wordInWord(bigword, smallword){

	p1 = getPrimeCode(bigword);
	p2 = getPrimeCode(smallword);

	if (p2>p1){
		return false;
	}

	if (p1%p2 == 0){

		return true;
	}

	return false;

}

function getWordsOfLength(arr, L){
	f = function(x){
		return x.length == L;
	}

	return arr.filter(f);
}

function getValidWords(word){

	validWords = [];
	for (var i=0; i<WORDLIST.length; i++){
		dword = WORDLIST[i];
		if (wordInWord(word, dword) == true){
			validWords.push(dword);
		}		
	}
	return validWords;
}

function npeat_array(val, N){
	arr = new Array(N);

	for (var i=0; i<N; i++){
		arr[i] = val;
	}

	return arr;

}

Array.prototype.shuffle_sub_array = function(indices){

	sub_arr = [];

	for (var i=0; i<indices.length; i++){
		sub_arr.push(this[indices[i]]);
	}

	sub_arr.shuffle();


	for (var i=0; i<indices.length; i++){
		this[indices[i]] = sub_arr[i];
	}
}

function getWordValue(word){
	var val = 0.0;

	for (var i=0; i<word.length; i++){
		val += TILE_VALUES[word[i]];
	}

	return val;
}

