createjs.Text.snapToPixel = true;
createjs.Shape.snapToPixel = true;
createjs.Container.snapToPixel = true;

(function() { 
function ScoreBoard(x, y, prefix, score) {

	this.Container_constructor();
	this.x = x;
	this.y = y;
	this.prefix = prefix;
	this.score = score;
	this.setup();
}

var p = createjs.extend(ScoreBoard, createjs.Container);

p.setup = function(){

	this.scoretxt = new createjs.Text("",
									 SCOREBOARD_FONT_SIZE.toString() + "px " + SCOREBOARD_FONT,
									 SCOREBOARD_COLOR);

	this.scoretxt.textBaseline = "middle";
	this.scoretxt.textAlign = "center";
	this.updateScore(this.score);

	this.addChild(this.scoretxt);
}


p.updateScore = function(score){
	
	this.score = score;
	this.scoretxt.text = this.prefix + this.score.toString();
}

window.ScoreBoard = createjs.promote(ScoreBoard, "Container");

}());

(function() { 
function WordPanel(validWords, remainingWords,y) {

	this.Container_constructor();
	this.width = stage.canvas.width*0.95;
	this.x = 0.5*(stage.canvas.width - this.width);
	this.y = y;
	this.validWords = validWords.sort();
	this.remainingWords = remainingWords.sort();
	this.hidden = true;

	this.setup();
}

var p = createjs.extend(WordPanel, createjs.Container);

p.setup = function(){

	this.children = [];

	var s = "";
	if (this.validWords.length == 0){
		s = "NONE";
	}
	else{
		var wordlength = this.validWords[0].length;

		s += wordlength.toString() + " LETTER WORDS:\n";

		for (var i=0; i<this.validWords.length; i++){
			var word = this.validWords[i];
			if (this.remainingWords.indexOf(word) >= 0){
				s += "-".repeat(word.length);
			}
			else{
				s += word;
			}

			if (i<this.validWords.length-1){
				s += "  ";
			}
		}
	}	
	this.panel_text = new createjs.Text(s,
									 WORDPANEL_FONT_SIZE.toString() + "px " + WORDPANEL_FONT,
									 SCOREBOARD_COLOR);

	this.panel_text.lineWidth = this.width*0.9;
	this.panel_text.lineHeight = WORDPANEL_FONT_SIZE*2;
	this.panel_text.x = 0.5*(this.width-this.panel_text.lineWidth);
	var bounds = this.panel_text.getBounds();
	var panel_height = bounds.height*1.2;
	this.panel_text.y = WORDPANEL_FONT_SIZE;

	this.panel_background = new createjs.Shape();
	
	this.panel_background.graphics.beginFill(BUTTONFACE_COLOR).drawRect(0, 
														  0, 
														  this.width, 
														  panel_height);

	this.panel_background.alpha = WORDPANEL_ALPHA;

	this.addChild(this.panel_background);
	this.addChild(this.panel_text);
	this.y = 0;
	this.cache(0, this.y, stage.canvas.width, panel_height);
	this.y = -500;
	this.hitArea = this.panel_background;

}

p.hide = function(){

	if (! this.hidden){
		//console.log('panel up');
		createjs.Tween.get(this)
		  .to({x: this.x, y: -500 }, 200, createjs.Ease.linear);
		  this.hidden = true;}
}
p.show = function(){

	if (this.hidden){
		//console.log('panel down');
		this.visible = true;
		createjs.Tween.get(this)
		  .to({x: this.x, y: 0 }, 200, createjs.Ease.linear);
		  this.hidden = false;}
	
}
window.WordPanel = createjs.promote(WordPanel, "Container");

}());

(function() { 
function Overlay(x, y, width, height, msg) {

	this.Container_constructor();
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.msg = msg;
	this.setup();
}

var p = createjs.extend(Overlay, createjs.Container);

p.setup = function(){
	var s = this.msg;

	this.overlay_text = new createjs.Text(s, 
									OVERLAY_FONT_SIZE.toString() + 'px ' + OVERLAY_FONT,
									OVERLAY_FONT_COLOR);

	this.overlay_text.x = this.width/2.0;
	this.overlay_text.y = this.height/3.0;
	this.overlay_text.textAlign = "center";

	this.overlay_background = new createjs.Shape();
	this.overlay_background.graphics.beginFill(OVERLAY_COLOR).drawRect(0, 0, this.width, this.height);

	this.overlay_background.alpha = OVERLAY_ALPHA;

	this.addChild(this.overlay_background);
	this.addChild(this.overlay_text);

	this.cache(0,0,this.width,this.height);

}

window.Overlay = createjs.promote(Overlay, "Container");

}());


(function() { 
function GameTimer(time) {

	this.startTime = time;
	this.Container_constructor();
	this.x = stage.canvas.width/2.0;
	this.y = GAMETIMER_Y;
	this.time = time*FPS;
	this.hurry = false;
	this.setup();
	
}

var p = createjs.extend(GameTimer, createjs.Container);

p.setup = function(){

	var gametimer_string = this.getTimeSeconds();

	this.gametimer_txt = new createjs.Text(gametimer_string,
									 GAMETIMER_FONT,
									 GAMETIMER_COLOR);

	this.gametimer_txt.textBaseline = "middle";
	this.gametimer_txt.textAlign = "center";

	this.addChild(this.gametimer_txt);

	var myThis = this;

	this.handleTick = function(event){
		if (createjs.Ticker.paused){
				game.stophurry();
				return;}

		var time = myThis.time - 1;
		
		myThis.updateGameTimer(time);
		if (myThis.getTimeSeconds() <= 10){
				game.hurry();}
		else{
				game.stophurry();}

		if (myThis.getTimeSeconds() <= 0.0){
			game.gameover();}
	}

}

p.restart = function(){
	this.time = this.startTime*FPS;
}

p.getTimeSeconds = function(){
	var time = Math.round(this.time/60.0);
	if (time < 0){
		time = 0;}

	return time;
}

p.updateGameTimer = function(time){
	
	this.time = time;
	this.gametimer_txt.text =this.getTimeSeconds().toString();
}

p.addTime = function(time){
	this.updateGameTimer(this.time + time*FPS);
}


window.GameTimer = createjs.promote(GameTimer, "Container");

}());

(function() { 
function Button(label, x, y, width, height) {

	this.Container_constructor();
	this.width = width;
	this.height = height;
	this.x = x-width/2.0;
	this.y = y;
	this.label = label;
	this.labelsize = BUTTON_FONT_SIZE;
	this.setup();
}

var p = createjs.extend(Button, createjs.Container);

p.setup = function(){
	var buttonface = new createjs.Shape();
	var buttonborder = new createjs.Shape();
	
	buttonface.graphics.beginFill(BUTTONFACE_COLOR).drawRect(0, 
														  0, 
														  this.width, 
														  this.height);

	buttonface.graphics.beginStroke(BUTTON_BORDER_COLOR).drawRect(0,
															  0,
															  this.width,
															  this.height);

	buttonborder.graphics.beginStroke(BUTTON_BORDER_COLOR).drawRect(0,
															  0,
															  this.width,
															  this.height);
	buttonface.alpha = BUTTON_ALPHA;

	this.addChild(buttonborder);
	this.addChild(buttonface);

	var labelfont =  this.labelsize.toString() + "px " + SCOREBOARD_FONT;
	var labelface = new createjs.Text(this.label,
									   labelfont,
									   SCOREBOARD_COLOR);

	labelface.x = this.width/2.0;
	labelface.y = this.height/2.0;
	labelface.textAlign = "center";
	labelface.textBaseline = "middle";

	this.addChild(labelface);
}



window.Button = createjs.promote(Button, "Container");

}());

// EXTEND THE CREATEJS CONTAINER CLASS TO CREATE A TILE OBJECT

(function() { 
function Tile(letter, x0, y0){

	this.Container_constructor();
	this.x = x0;
	this.y = y0;
	this.letter = letter;
	this.score = TILE_VALUES[letter];
	this.setup();
	this.selected = false;
}

var p = createjs.extend(Tile, createjs.Container);

p.setup = function(){

	// Add letter to tile square
	var tileface = new createjs.Shape();
	tileface.graphics.beginFill(TILE_FILL_COLOR).drawRect(0, 
														  0, 
														  TILE_WIDTH, 
														  TILE_WIDTH)
	tileface.graphics.beginStroke(TILE_BORDER_COLOR).drawRect(0,
															  0,
															  TILE_WIDTH,
															  TILE_WIDTH);

	this.addChild(tileface);

	var letterfont = TILE_LETTER_WIDTH.toString() + "px " + TILE_LETTER_FONT;
	var letterface = new createjs.Text(this.letter,
									   letterfont,
									   TILE_LETTER_COLOR);

	// position relative to tile
	letterface.x = TILE_WIDTH/2.0;
	letterface.y = TILE_WIDTH/2.0;
	letterface.textAlign = "center";
	letterface.textBaseline = "middle";

    this.addChild(letterface); 

    // add score to bottom right corner
    var scorefont = (TILE_LETTER_WIDTH/4.0).toString() + "px " + TILE_LETTER_FONT;
    var scoreface = new createjs.Text(this.score.toString(), 
    								  scorefont,
    								  TILE_LETTER_COLOR);

    scoreface.x = TILE_WIDTH*0.75;
    scoreface.y = scoreface.x;
    this.addChild(scoreface);

    var myThis = this;

    this.mouseChildren = false;

    this.select = function(){
    	if (! myThis.selected){
    		myThis.parent.select_tile(myThis);}
    	else{
    		return;
    	}
    }

    this.hitArea = tileface;
    this.on("click",this.select);

    this.cache(0,0,65,65);

}


p.move = function(x,y){

	var z = this.parent.children.length -1;
	this.parent.setChildIndex(this, z);
	//this.x = x;
	//this.y = y;
	//stage.update();

	createjs.Tween.get(this)
	  .to({ x: x, y: y }, 200, createjs.Ease.getPowInOut(2));

}


window.Tile = createjs.promote(Tile, "Container");

}());

// EXTEND THE CREATEJS CONTAINER CLASS TO MAKE A TILEBOARD OBJECT (USED FOR RACK AND BOARD)

(function() {
function TileBoard(word, x0, y0){

	this.Container_constructor();
	this.x = x0;
	this.y = y0;
	this.word = word;
	this.tiles = [];
	this.boardspaces = [];
	this.played_tiles = [];
	this.unplayed_tiles = [];
	this.setup_spaces();
	this.setup_rack();
	
}

var p = createjs.extend(TileBoard, createjs.Container);

p.setup_rack = function(){

	
	word = this.word.shuffle();
	for (var i=0; i<word.length; i++){
		var x = i*(TILE_WIDTH + TILE_SPACING);
		var y = BOARDSPACE_SEP;
		var tile = new Tile(word[i], x, y);
		this.tiles.push(tile)
		this.unplayed_tiles.push(tile);
		this.addChild(tile);
	}

}

p.setup_spaces = function(){

	N = this.word.length;

	for (var i=0; i<N; i++){

		var x = this.x + i*(TILE_WIDTH+TILE_BOX_BORDER_WIDTH);
		var y = Y_TILE_BOARD;

		var boardspace = new createjs.Shape();
		//boardspace.graphics.beginFill(TILE_BOX_FILL_COLOR).drawRect(0, 
		//													        0, 
		//													        TILE_WIDTH, 
		//													        TILE_WIDTH);

		boardspace.graphics.setStrokeStyle(TILE_BOX_BORDER_WIDTH);
		var W = TILE_WIDTH + TILE_BOX_BORDER_WIDTH;
		boardspace.graphics.beginStroke("yellow").drawRect(0, 
															   0, 
															   W, 
										                       W);
		
		boardspace.x = x;
		boardspace.y = y;
		this.boardspaces.push(boardspace);
		boardspace.visible = false;
		this.addChild(boardspace);
			
	}

}


p.select_tile = function(tile){
	
	if (game.paused){
		return;
	}

	if (tile && tile.selected == false){
		createjs.Sound.play(soundID_select_tile);
		tile.selected = true;
		ind = this.unplayed_tiles.indexOf(tile);
		this.unplayed_tiles.splice(ind, 1);
		this.played_tiles.push(tile);

		coords = this.getSpaceCoords(this.played_tiles.length-1);
		x = coords[0]; y = coords[1];
		tile.move(x,y);

		

		return 1;
	}	
	return 0;
}

p.replace_tile = function(tile, playsound)
{
	//console.log(tile);
	//console.log(playsound);
	// Pops the last placed tile back to where it came from

	if (playsound == null){
		playsound = true;
	}

	var ind_p = this.played_tiles.indexOf(tile);
	//console.log(ind_p);
	if (ind_p < 0){
		return;
	}

	if (playsound){
		createjs.Sound.play(soundID_select_tile);
	}

	this.unplayed_tiles.push(tile);
	this.played_tiles.splice(ind_p, 1);
	var ind = this.tiles.indexOf(tile);
    var coords = this.getRackCoords(ind);
    x = coords[0]; y = coords[1];
    //console.log('move');
    tile.move(x,y);
    tile.selected = false;
	
}

p.replace_last_tile = function(playsound){
	//console.log('last tile');
	var L = this.played_tiles.length;
	if (L > 0){
		tile = this.played_tiles[L-1];
		this.replace_tile(tile, playsound);}
}

p.spacepop = function(playsound){
	// Pops the last placed tile back to where it came from


	if (playsound === undefined){
		playsound = true;
	}

	L = this.played_tiles.length;
	if (L > 0){
		if (playsound){
			createjs.Sound.play(soundID_select_tile);
		}
		tile = this.played_tiles.pop();
		this.unplayed_tiles.push(tile);
		ind = this.tiles.indexOf(tile);
	    coords = this.getRackCoords(ind);
	    x = coords[0]; y = coords[1];
	    tile.move(x,y);
	    tile.selected = false;
	}
}

p.shuffle = function(){

	var initial_rack = this.getString(this.unplayed_tiles);

	if (initial_rack.length<=1){
		return;
	}

	var new_rack = initial_rack.slice();

	while (initial_rack == new_rack){

		createjs.Sound.play(soundID_shuffle_tiles);
		var j = 0;
		this.unplayed_tiles.shuffle();
		var shuffled_tiles = [];
		for (var i=0; i<this.tiles.length; i++){
			if (this.unplayed_tiles.indexOf(this.tiles[i]) > -1){
				shuffled_tiles.push(this.unplayed_tiles[j]);
				j += 1;

			}
			else{
				shuffled_tiles.push(this.tiles[i]);
			}
			
		}

		new_rack = this.getString(this.unplayed_tiles);
	}
	this.tiles = shuffled_tiles
	for (var i=0; i<this.unplayed_tiles.length; i++){
		var ind = this.tiles.indexOf(this.unplayed_tiles[i]);
		coords = this.getRackCoords(ind);
		x = coords[0]; y = coords[1];
		this.unplayed_tiles[i].move(x,y);
	}

	//console.log(initial_rack);
	//console.log(new_rack);
}

p.clear = function(){

	if (this.played_tiles.length == 0){
		return
	}

	while (this.played_tiles.length>0){
		this.spacepop(false);
	}
	
}

p.getSpaceCoords = function(i){

	x = this.boardspaces[i].x;
	y = this.boardspaces[i].y;

	x += TILE_BOX_BORDER_WIDTH/2.0;
	y += TILE_BOX_BORDER_WIDTH/2.0;

	return [x, y];
}

p.getRackCoords = function(i){

	var y = BOARDSPACE_SEP;
	var x = i*(TILE_WIDTH + TILE_SPACING);
	return [x,y];
}

p.getTile = function(tile_arr, letter){

	for (var i=0; i<tile_arr.length; i++){
		if (tile_arr[i].letter == letter){
			return tile_arr[i];
		}
	}

	return null;

}

p.getString = function(tile_arr){
	s = ""
	for (var i=0; i<tile_arr.length; i++){
		s = s + tile_arr[i].letter;
	}
	return s
}

p.getWidth = function(){
	return (this.word.length)*TILE_WIDTH + (this.word.length-1)*TILE_SPACING;
}

p.getHeight = function(){
	return TILE_WIDTH;
}

window.TileBoard = createjs.promote(TileBoard, "Container");

}());

function Game(word){
	
	this.score = 0.0;
	this.round = 1;
	this.words7 = getWordsOfLength(WORDLIST, 7);
	this.setup();
	this.paused = false;
}
Game.prototype.setup = function(){

	    // load sounds
	    this.loadSounds();		

	    // load high score
	    if (! (localStorage.highscore > 0)){
	    	localStorage.highscore = 0;
	    }
	    if (! (localStorage.highround>1)){
	    	localStorage.highround = 1;
	    }

		// Get random 7 letter word from dictionary
		this.currentWord = this.randomWord();
		//console.log(this.currentWord);
		//this.currentWord = "MUGGERS";
		this.validWords = getValidWords(this.currentWord);
		this.remainingWords = this.validWords.slice();
		this.scoredWords = [];

		rack = new TileBoard(this.currentWord, X_TILE_BOARD, Y_TILE_BOARD);
		rack.x = (c.width - rack.getWidth())/2;
		rack.y = (c.height - rack.getHeight())/2;
				
		scoreboard = new ScoreBoard(SCOREBOARD_X, SCOREBOARD_Y,
									"SCORE: ", 0);
		scoreboard.scoretxt.textAlign = 'right';

		high_scoreboard = new ScoreBoard(HIGHSCORE_X, 
										 HIGHSCORE_Y,
										 "HIGH SCORE: ", parseInt(localStorage.highscore));
		high_scoreboard.scoretxt.textAlign = 'right';
		high_scoreboard.scoretxt.color = HIGHSCORE_COLOR;

		roundtracker = new ScoreBoard(stage.canvas.width - SCOREBOARD_X,
										SCOREBOARD_Y, "ROUND ", 1);

		roundtracker.scoretxt.textAlign = 'left';
		

		high_roundtracker = new ScoreBoard(HIGHROUND_X,
										   HIGHROUND_Y,
										   "HIGH ROUND: ", parseInt(localStorage.highround))
		high_roundtracker.scoretxt.textAlign = 'left';
		high_roundtracker.scoretxt.color = HIGHSCORE_COLOR;

		gametimer = new GameTimer(START_TIME);


		submit_button = new Button("SUBMIT", 
									stage.canvas.width/2.0 + 0.75*BUTTON_WIDTH,
									SUBMIT_BUTTON_Y,
									BUTTON_WIDTH,
									BUTTON_HEIGHT);

		shuffle_button = new Button("SHUFFLE", 
									stage.canvas.width/2.0 - 0.75*BUTTON_WIDTH,
									SUBMIT_BUTTON_Y,
									BUTTON_WIDTH,
									BUTTON_HEIGHT);

		giveup_button = new Button("GIVE UP", 
									stage.canvas.width/2.0,
									GIVEUP_BUTTON_Y,
									BUTTON_WIDTH,
									BUTTON_HEIGHT);

		var pause_msg = "Paused\n\nPress ESC to Resume";
		pause_overlay = new Overlay(0,0,stage.canvas.width, stage.canvas.height,pause_msg);
		pause_overlay.visible = false;

		var gameover_msg = "Game Over\nClick anywhere to play again.";
		gameover_overlay = new Overlay(0,0,stage.canvas.width, stage.canvas.height,gameover_msg);
		gameover_overlay.visible = false;

		var intro_msg = "Welcome to Scrabble Twist!\n\nClick To Play";
		intro_overlay = new Overlay(0,0,stage.canvas.width, stage.canvas.height, intro_msg);
		intro_overlay.visible = true;

		this.suspend();
		pause_overlay.visible = false;

		stage.addChild(rack, scoreboard, roundtracker, gametimer, 
					   submit_button, shuffle_button, giveup_button,
					   high_scoreboard, high_roundtracker, pause_overlay, gameover_overlay,
					   intro_overlay);



		// Make Word Panels
		this.wordpanels = [];
		for (var i=2; i<=MAX_WORD_LENGTH; i++){
			var validWords = getWordsOfLength(this.validWords, i);
			var remainingWords = validWords.slice();
			var wordpanel = new WordPanel(validWords, remainingWords);
			wordpanel.visible = false;
			wordpanel.y = 0;
			this.wordpanels.push(wordpanel);
			stage.addChild(this.wordpanels[i-2]);
			//this.wordpanels[i-2].visible = false;
		}

	}

Game.prototype.updateScore = function(score){
	this.score = score;
	scoreboard.updateScore(score);
}

Game.prototype.next_round = function(){
	// Get random 7 letter word from dictionary
	this.currentWord = this.randomWord();
	//this.currentWord = "MUGGERS";
	this.validWords = getValidWords(this.currentWord);
	this.remainingWords = this.validWords.slice();
	this.scoredWords = [];
	
	rack = new TileBoard(this.currentWord, X_TILE_BOARD, Y_TILE_BOARD);
	rack.x = (c.width - rack.getWidth())/2;
    rack.y = (c.height - rack.getHeight())/2;
	stage.children[0] = rack;
	roundtracker.updateScore(this.round);
	this.updateWordPanels();
	if (gametimer.getTimeSeconds() < START_TIME){
		gametimer.restart();
	}
}

Game.prototype.updateRound = function(round){
	this.round = round;
	roundtracker.updateScore(round);
}

Game.prototype.new_game = function(){
	this.updateRound(1);
	this.updateScore(0);
	this.next_round();
	gametimer.restart();
	gameover_overlay.visible = false;
	console.log(gameover_overlay.visible);
	this.resume();
}

Game.prototype.randomWord = function(){
		return this.words7.draw();
	}

Game.prototype.loadSounds = function() {
	  createjs.Sound.registerSound("Sounds/select_tile.mp3", soundID_select_tile);
	  createjs.Sound.registerSound("Sounds/shuffle_tiles.mp3", soundID_shuffle_tiles);
	  createjs.Sound.registerSound("Sounds/submit_word.mp3", soundID_submit_word);
	  createjs.Sound.registerSound("Sounds/bad_word.mp3", soundID_bad_word);
	  createjs.Sound.registerSound("Sounds/bingo.mp3", soundID_bingo);
	  createjs.Sound.registerSound("Sounds/hurry.mp3", soundID_hurry);
	  createjs.Sound.registerSound("Sounds/gameover.mp3", soundID_gameover);
	}

Game.prototype.checkWord = function(word){
/*
	code indicates validity of play
	code = -1  -   invalid word
	code = 0   -   valid but already played
	code = 1   -   score!
*/
	var code = -1; 

	if (this.validWords.indexOf(word) > -1){
		code += 1;
	}

	if (this.remainingWords.indexOf(word) > -1){
		code += 1;
		if (word.length == 7){
			code += 1;
		}
	}



	return code;
}

Game.prototype.stophurry = function(){
	if (gametimer.hurry){
		gametimer.hurry = false;
		createjs.Sound.stop();}
}

Game.prototype.hurry = function(){
	if (! gametimer.hurry){
		gametimer.hurry = true;
		createjs.Sound.play(soundID_hurry);}
}

Game.prototype.bingo = function(){
	createjs.Sound.play(soundID_bingo);
	this.round +=1;
	alert("You Win! Moving on to Round " + this.round.toString());
	this.next_round();
	

}

Game.prototype.play = function(){

	var word = rack.getString(rack.played_tiles);
	var code = this.checkWord(word);

	if (code == -1){
		// play bad sound
		createjs.Sound.play(soundID_bad_word);
	}
	else if (code > 0){
		//console.log("Playing sound");
		createjs.Sound.play(soundID_submit_word);
		this.updateScore(this.score + getWordValue(word));
		this.scoredWords.push(word);
		this.remainingWords.splice(this.remainingWords.indexOf(word), 1);
		this.updateWordPanels();
	}

	if (code == 1){
		gametimer.addTime(getWordValue(word));
	}

	if (code == 2){
		this.bingo();
	}

	rack.clear()
	this.checkHighScores();
}

Game.prototype.missed_bingos = function(){
	var arr = getWordsOfLength(this.remainingWords, 7);
	s = "";
	for (var i=0; i<arr.length; i++){
		s += "\n";
		s += arr[i];
	}

	return s;
}

Game.prototype.gameover = function(){
	
	game.suspend();
	game.stophurry();
	pause_overlay.visible = false;
	gameover_overlay.overlay_text.text = gameover_overlay.msg +  "\n\nYou Missed:" + this.missed_bingos();
	gameover_overlay.cache(gameover_overlay.x,gameover_overlay.y,
						   gameover_overlay.width, gameover_overlay.height);
	
	gameover_overlay.visible = true;
	stage.update();
	var instance = createjs.Sound.play(soundID_gameover);


}

Game.prototype.updateWordPanels = function(){
	for (var i=2; i<=MAX_WORD_LENGTH; i++){
		var validWords = getWordsOfLength(this.validWords, i);
		var remainingWords = getWordsOfLength(this.remainingWords, i);
		this.wordpanels[i-2].validWords = validWords;
		this.wordpanels[i-2].remainingWords = remainingWords;
		this.wordpanels[i-2].setup();
	}
}

Game.prototype.showWordPanel = function(n){
	//this.wordpanels[n-2].visible = true;
	if (n>=2 && n<=7){
	this.wordpanels[n-2].show();}
}

Game.prototype.hideWordPanel = function(n){
	//this.wordpanels[n-2].visible = false;
	if (n>=2 && n<=7){
	this.wordpanels[n-2].hide();}
}

Game.prototype.checkHighScores = function(){
	if (this.score > parseInt(localStorage.highscore)){
		localStorage.highscore = this.score;
		high_scoreboard.updateScore(this.score);
	}
	if (this.round > parseInt(localStorage.highround)){
		localStorage.highround = this.round;
		high_roundtracker.updateScore(this.round);
	}
}

Game.prototype.suspend = function(){
	//console.log('suspend');
	pause_overlay.visible = true;
	this.paused = true;
	createjs.Ticker.paused = true;
}

Game.prototype.resume = function(){
	//console.log('resume');
	pause_overlay.visible = false;
	this.paused = false;
	createjs.Ticker.paused = false;
}



var soundID_select_tile = "sound_select_tile";
var soundID_shuffle_tiles = "sound_shuffle_tiles";
var soundID_submit_word = "sound_submit_word";
var soundID_bad_word = "sound_bad_word";
var soundID_bingo = "bingo";
var soundID_hurry = "hurry";
var soundID_gameover = "gameover";

var c = document.getElementById("myCanvas");
var stage = new createjs.Stage(c);
createjs.Touch.enable(stage);
stage.snapToPixelEnabled = true;

game = new Game();

createjs.Ticker.setFPS(FPS);
createjs.Ticker.timingMode = createjs.Ticker.RAF;
createjs.Ticker.addEventListener("tick", stage);
createjs.Ticker.addEventListener("tick", gametimer.handleTick);


function doKeyDown(e) {
		if (e.keyCode == 27){ //ESCAPE
			//console.log('escape');
			if (! game.paused){
				game.suspend();
			}
			else{
				game.resume();
			}
			return;
			e.preventDefault();
		}
		if (game.paused){
			return;
		}
		if (e.keyCode === 8){ //BACKSPACE
			rack.replace_last_tile(true);
			e.preventDefault();
		}

		else if (e.keyCode === 13){ //ENTER
			game.play();
			e.preventDefault();
		}
		else if (e.keyCode === 32){
			rack.shuffle();
			e.preventDefault();
		} 
		else{

			key_pressed = e.keyCode;
			key_pressed = String.fromCharCode(key_pressed);

			var numeric = parseInt(key_pressed);

			if (!isNaN(numeric) && (2<=numeric<=7)){
				game.showWordPanel(numeric);
			}
			else{

				key_pressed = key_pressed.toUpperCase();
				rack.select_tile(rack.getTile(rack.unplayed_tiles,key_pressed));
				//console.log(key_pressed);
			}
			e.preventDefault();
	}
}

function doKeyUp(e) {
		if (game.paused){
			return;
		}	
		//console.log(e.keyCode);

		key_pressed = e.keyCode;
		key_pressed = String.fromCharCode(key_pressed);

		var numeric = parseInt(key_pressed);

			if (!isNaN(numeric) && (2<=numeric<=7)){
				game.hideWordPanel(parseInt(key_pressed));
		  }
	
}

function doSubmit(e){
	if (! game.paused){
	game.play();}
}

function doShuffle(e){
	if (! game.paused){
	rack.shuffle();}
}

function doGameOver(e){
	if (! game.paused){
	game.gameover();}
}


shuffle_button.on("click", doShuffle);
submit_button.on("click", doSubmit);
giveup_button.on("click", doGameOver);

gameover_overlay.on("click", game.new_game.bind(game));
intro_overlay.on("click", function(){
				          game.new_game();
				          intro_overlay.visible = false;});

window.addEventListener("keydown", doKeyDown, false );
window.addEventListener("keyup", doKeyUp, false );

stage.on("stagemousedown", function(event){
	var x = event.stageX;
	var y = event.stageY;
	if (y < stage.canvas.height/2.0){
		rack.replace_last_tile();
	}
})

//stage.update();
